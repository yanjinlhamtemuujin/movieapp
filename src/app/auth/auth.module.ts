import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { AuthRoutingModule } from './auth-routing.module';
import { LoginComponent } from './component/login/login.component';
import { RegisterComponent } from './component/register/register.component';
import { ResetPasswordComponent } from './component/resetpassword/reset-password.component';
import {ToastModule} from "primeng/toast";
import {BrowserAnimationsModule} from "@angular/platform-browser/animations";
import {KeyFilterModule} from "primeng/keyfilter";

@NgModule({
  declarations: [
    LoginComponent,
    RegisterComponent,
    ResetPasswordComponent,
  ],
  exports: [
    LoginComponent,
    RegisterComponent,
    ResetPasswordComponent,
    ToastModule,
    BrowserAnimationsModule
  ],
    imports: [
        CommonModule,
        AuthRoutingModule,
        FormsModule,
        ToastModule,
        BrowserAnimationsModule,
        KeyFilterModule,
    ]
})
export class AuthModule { }
