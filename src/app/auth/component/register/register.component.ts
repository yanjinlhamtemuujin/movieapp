import { Component, OnInit } from '@angular/core';
import {MovieServiceService} from "../../../../movie-service.service";
import {MessageService} from "primeng/api";
import {Router} from "@angular/router";
import {GLOBAL} from "../../../constant/GLOBAL";

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {

  data: any;
  constructor(private httpService: MovieServiceService,
              private messageService: MessageService,
              private router: Router) { }

  ngOnInit(): void {
    this.initData()
  }

  initData() {
    this.data = {
      userName: '',
      firstName:  '',
      lastName: '',
      email: '',
      password: ''
    }
  }

  register(){
    if (this.data.userName == '') {
      GLOBAL.showWarning("username is empty", this.messageService);
      return;
    }

    if (this.data.firstName == '') {
      GLOBAL.showWarning("firstName is empty", this.messageService);
      return;
    }

    if (this.data.lastName == '') {
      GLOBAL.showWarning("lastName is empty", this.messageService);
      return;
    }

    if (this.data.email == '') {
      GLOBAL.showWarning("email is empty", this.messageService);
      return;
    }

    if (this.data.password == '') {
      GLOBAL.showWarning("password is empty", this.messageService);
      return;
    }

    this.httpService.addUser(this.data).subscribe( result => {
      GLOBAL.showSuccess("Амжилттай хадгаллаа", this.messageService);
      console.log(this.data);
      setTimeout(() => {
        this.router.navigate(['login/'], {skipLocationChange: false});
      },1000);
    },
      error => {
        GLOBAL.showWarning('И-мэйл хаяг дээр бүртгэлтээ байна', this.messageService);
      });
  }
}
