import { Component, OnInit } from '@angular/core';
import {MovieServiceService} from "../../../movie-service.service";
import {ActivatedRoute, Router} from "@angular/router";
import {MessageService} from "primeng/api";
import {GLOBAL} from "../../constant/GLOBAL";

@Component({
  selector: 'app-upcoming',
  templateUrl: './upcoming.component.html',
  styleUrls: ['./upcoming.component.css']
})
export class UpcomingComponent implements OnInit {

  page: any;
  movies: any[] = [];
  userInfo: any;


  constructor(private httpService: MovieServiceService,
              private route: ActivatedRoute,
              private router: Router,
              private messageService: MessageService) {
    this.route.params.subscribe(result => {
      this.page = result.page;
    });
  }


  ngOnInit(): void {
    // @ts-ignore
    this.userInfo = JSON.parse(localStorage.getItem('user'));

    this.httpService.getUpcoming(this.page).subscribe(result => {
      this.movies = result.results;
      let imageUrl = 'https://image.tmdb.org/t/p/w500';

      this.movies.forEach(it => {
        it.poster_path = imageUrl + it.poster_path;
      })
    });
  }


  readMore(id: number) {
    this.router.navigate(['readmore/' + id], {skipLocationChange: false});
  }

  addToWatchlist(movie: any) {
    GLOBAL.addToWatchlist(movie, this.userInfo,this.httpService , this.router, this.messageService)
  }

}
