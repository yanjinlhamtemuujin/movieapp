import { Component, OnInit } from '@angular/core';
import {MovieServiceService} from "../../../movie-service.service";
import {ActivatedRoute, Router} from "@angular/router";
import {GLOBAL} from "../../constant/GLOBAL";
import {MessageService} from "primeng/api";

@Component({
  selector: 'app-watchlist',
  templateUrl: './watchlist.component.html',
  styleUrls: ['./watchlist.component.css']
})
export class WatchlistComponent implements OnInit {

  userInfo: any;
  watchlist: any[] = [];

  constructor(private httpService: MovieServiceService,
              private route: ActivatedRoute,
              private router: Router,
              private messageService: MessageService) {
  }

  ngOnInit(): void {
    // @ts-ignore
    this.userInfo = JSON.parse(localStorage.getItem('user'));
    console.log(this.userInfo);


    this.httpService.getWatchlist(this.userInfo.userId).subscribe(result => {
      this.watchlist = result;
        console.log("watchlist:     ", this.watchlist);
    },
      error => {
      GLOBAL.showWarning("asdf", this.messageService);
      });

  }

  readMore(id: number) {
    this.router.navigate(['readmore/' + id], {skipLocationChange: false});
  }

}
