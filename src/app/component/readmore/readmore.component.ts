import { Component, OnInit } from '@angular/core';
import {MovieServiceService} from "../../../movie-service.service";
import {ActivatedRoute, Route, Router} from "@angular/router";
import {forkJoin, Observable} from "rxjs";
import {DomSanitizer, SafeResourceUrl} from '@angular/platform-browser';
import {MenuItem, MessageService} from "primeng/api";
import {GLOBAL} from "../../constant/GLOBAL";


@Component({
  selector: 'app-readmore',
  templateUrl: './readmore.component.html',
  styleUrls: ['./readmore.component.css']
})
export class ReadmoreComponent implements OnInit {
  data: any;
  credits: any;
  movieId: any;
  userId: any;
  cast: any[] = [];
  crew: any[] = [];
  videos: any;
  similars: any[] = [];
  othersreviews: any[] = [];
  allReviews: any[] = [];
  responsiveOptions: any;
  val1: number = 5;
  stars: number = 1;
  review: any;
  rating: number = 0;
  userInfo: any;
  localStorageIsFree: boolean = false;

  constructor(private httpService: MovieServiceService,
              private route: ActivatedRoute,
              private sanitizer: DomSanitizer,
              private router: Router,
              private messageService: MessageService) {

    this.route.params.subscribe(result => {
      this.movieId = result.movieId;
    });
    this.
      responsiveOptions = [
      {
        breakpoint: '1024px',
        numVisible: 3,
        numScroll: 3
      },
      {
        breakpoint: '768px',
        numVisible: 2,
        numScroll: 2
      },
      {
        breakpoint: '560px',
        numVisible: 1,
        numScroll: 1
      }
    ];

    this.router.routeReuseStrategy.shouldReuseRoute = () => false;
  }

  ngOnInit(): void {
    this.initData();

    let getOne = this.httpService.getOne(this.movieId);
    let getCredits = this.httpService.getCredits(this.movieId);
    let getVideos = this.httpService.getVideos(this.movieId);
    let getSimilar = this.httpService.getSimilar(this.movieId);
    let getOthersReviews = this.httpService.getOthersReviews(this.movieId);

    forkJoin([getOne, getCredits, getVideos, getSimilar]).subscribe(results => {
      this.data = results[0];
      this.data.poster_path = 'https://image.tmdb.org/t/p/w500' + this.data.poster_path;
      this.credits = results[1].crew;
      this.cast = results[1].cast;
      this.videos = results[2].results;
      this.similars = results[3].results;
      // this.othersreviews = results[4];
      console.log('othersreviews: ', this.othersreviews);

      this.videos.forEach((it: { videoUrl: SafeResourceUrl; key: string; }) => {
        it.videoUrl = this.sanitizer.bypassSecurityTrustResourceUrl('https://www.youtube.com/embed/' + it.key);
      });

      this.similars.forEach( it => {
        it.poster_path = 'https://image.tmdb.org/t/p/w500' + it.poster_path;
        this.val1 = Math.floor(it.vote_average / 2);
      })

      this.getOthersReviews();
    });
  }

  getOthersReviews() {
    this.httpService.getOthersReviews(this.movieId).subscribe( result => {
      this.othersreviews = result;
    })
  }

  readMore(id: number) {
    this.router.navigate(['readmore/' + id]);
  }

  initData() {
    this.review = {
      reviewId: 0,
      movieId: 0,
      userId: 0,
      headline: '',
      content: '',
      rating: 0
    }
  }

  saveReview() {
    if (this.review.content == '') {
      GLOBAL.showWarning("Review textarea is empty", this.messageService);
      return;
    }

    if (this.review.headline == '') {
      GLOBAL.showWarning("Headline textarea is empty", this.messageService);
      return;
    }
    // @ts-ignore
    this.userInfo = JSON.parse(localStorage.getItem('user'));
    this.localStorageIsFree = this.userInfo == null;

    if (this.localStorageIsFree) {
      GLOBAL.showWarning("You are not logged id", this.messageService);
      setTimeout(() => {
        this.router.navigate(['login/'], {skipLocationChange: false});
      },1000)
      return;
    }
    this.review.movieId = parseInt(this.movieId);
    this.review.rating = this.rating;
    this.review.userId = this.userInfo.userId;
    this.review.userName = this.userInfo.userName;

    this.httpService.addReview(this.review).subscribe( result => {
      GLOBAL.showSuccess("Амжилттай хадгаллаа", this.messageService);
      this.getOthersReviews();
    });
  }

  clearFields() {
    // @ts-ignore
    document.getElementById('saveRevieww').value=null;
  }
  //
  // addToWatchlist() {
  //   // @ts-ignore
  //   this.userInfo = JSON.parse(localStorage.getItem('user'));
  //   GLOBAL.addToWatchlist(this.data, this.userInfo, this.httpService , this.router, this.messageService)
  // }

  addToWatchlist(movie: any) {
    // @ts-ignore
    this.userInfo = JSON.parse(localStorage.getItem('user'));
    GLOBAL.addToWatchlist(movie, this.userInfo,this.httpService , this.router, this.messageService)
  }

}
