import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { HomeComponent } from './component/home/home.component';
import { ReadmoreComponent} from "./component/readmore/readmore.component";
import {PopularComponent} from "./component/popular/popular.component";
import {WatchlistComponent} from "./component/watchlist/watchlist.component";
import {UpcomingComponent} from "./component/upcoming/upcoming.component";
import {TopratedComponent} from "./component/toprated/toprated.component";


const routes: Routes = [
  { path: '', redirectTo: '/home', pathMatch: 'full'},
  { path: 'home', component: HomeComponent},
  { path: 'readmore/:movieId', component: ReadmoreComponent},
  { path: 'popular/:page', component: PopularComponent},
  { path: 'upcoming/:page', component: UpcomingComponent},
  { path: 'toprated/:page', component: TopratedComponent},
  { path: 'myWatchlist', component: WatchlistComponent},
];

@NgModule({
  imports: [RouterModule.forRoot(routes, {onSameUrlNavigation: 'reload'})],
  exports: [RouterModule]
})
export class AppRoutingModule { }
