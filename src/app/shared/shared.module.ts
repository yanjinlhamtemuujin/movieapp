import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HeaderComponent } from './component/header/header.component';
import {MenubarModule} from "primeng/menubar";
import {RouterModule} from "@angular/router";
import {MenuModule} from "primeng/menu";



@NgModule({
  declarations: [
    HeaderComponent
  ],
    imports: [
        CommonModule,
        MenubarModule,
        RouterModule,
        MenuModule
    ],
  exports: [
    HeaderComponent
  ]
})
export class SharedModule { }
