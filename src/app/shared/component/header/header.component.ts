import { Component, OnInit } from '@angular/core';
import {MenuItem} from "primeng/api";
import {ActivatedRoute, Router} from "@angular/router";
import {Emitter} from "./emitter";

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {
  userInfo: any;
  loggedin = false;
  localStorageIsFree = true;

  constructor(private router: Router,
              private route: ActivatedRoute) {
  }

  ngOnInit(): void {

    Emitter.authEmitter.subscribe(
      (auth: boolean) => {
        this.loggedin = auth;
      }
    )

    // @ts-ignore
    this.userInfo = JSON.parse(localStorage.getItem('user'));
    console.log(this.userInfo)
    this.localStorageIsFree = this.userInfo == null;
  }

  // login() {
  //   // window.location.reload();
  //   this.router.navigate() =this.route;
  // }

  logout() {
    Emitter.authEmitter.emit(false);
    localStorage.clear();
    this.localStorageIsFree = true;
    delete this.userInfo;
    this.router.navigate(['login/'], {skipLocationChange: false});
  }

  toWatchlist() {
    this.router.navigate(['myWatchlist/'], {skipLocationChange: false});
  }
}

