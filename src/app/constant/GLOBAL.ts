
export class GLOBAL {

  public static showSuccess(message: string, messageService: any) {
    messageService.add({severity:'success', summary: 'Амжилттай!', detail: message});
  }

  public static showWarning(message: string, messageService: any) {
    messageService.add({severity:'warn', summary: 'Анхааруулга!', detail: message});
  }

 public static addToWatchlist(movie: any, userInfo: any, httpService: any, router: any, messageService: any) {
    let {id,poster_path,title ,vote_average, overview } = movie;
    // @ts-ignore
    let localStorageIsFree : boolean= userInfo == null;

    if (localStorageIsFree) {
      this.showWarning("You are not logged in", messageService);
      setTimeout(() => {
        router.navigate(['login/'], {skipLocationChange: false});
      },1000)
      return;
    }
    let watchListMovie = {
      userId: userInfo.userId,
      movieId: id,
      path: poster_path,
      title: title,
      rating: vote_average,
      overview: overview
    }

    httpService.addToWatchlist(watchListMovie).subscribe(() => {
        this.showSuccess("Added to your Watchlist!", messageService);
        console.log(watchListMovie);
      },
      () => {
        this.showWarning("This movie is already in your Watchlist", messageService);
        return;
      });
  }
}
