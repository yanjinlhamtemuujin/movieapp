import { Injectable } from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {Observable, throwError} from "rxjs";
import {tap, catchError} from "rxjs/operators";
import {MessageService} from "primeng/api";


@Injectable({
  providedIn: 'root'
})
export class MovieServiceService {

  serviceUrl = 'https://api.themoviedb.org/3/movie/';
  apiKey = '75d7cdf320ca42bae05698564bcd728d';
  localUrl = 'http://localhost:8181/api/'

  topRated =  this.serviceUrl + 'top_rated?api_key=' + this.apiKey + '&language=en-US&page=';
  popular = this.serviceUrl + 'popular?api_key=' + this.apiKey + '&language=en-US&page=';
  upcoming = this.serviceUrl + 'upcoming?api_key=' + this.apiKey + '&language=en-US&page=';
  trending = 'https://api.themoviedb.org/3/trending/movie/week?api_key=' + this.apiKey;

  constructor(private  http: HttpClient,
              private messageService: MessageService) {
  }

  get(url: string): Observable<any[]> {
    return this.http.get<any>(url)
      .pipe(
        tap(data => console.log("fetched datas")
        ),
        catchError(this.handleError('getDatas', []))
      )
  }

  post(url: string, data: any, o:{ withCredentials: boolean }): Observable<any[]> {
    return this.http.post<any>(url,data)
      .pipe(
        tap(data => console.log("fetched datas")
        ),
        catchError(this.handleError('getDatas', []))
      )
  }

  handleError<T>(operation = 'operation', result?: T) {
    return (error: any): Observable<T> => {
      console.log('HandleError');

      let error_message = error.message;

      let returnObj = {
        url: error.url,
        message: error_message,
        status: error.status
      };
      return throwError(returnObj);
    };
  }

  getData():Observable<any> {
    return this.get(this.serviceUrl + 'genre/movie/list?api_key=' + this.apiKey)
  }

  getPopular(page: number):Observable<any> {
    return this.get(this.popular + page.toString())
  }

  getUpcoming(page: number):Observable<any> {
    return this.get(this.upcoming + page.toString())
  }

  getTopRated(page: number):Observable<any> {
    return this.get(this.topRated + page.toString())
  }

  getTrending():Observable<any> {
    return this.get(this.trending)
  }

  getOne(movieId: number): Observable<any> {
    return this.get(this.serviceUrl + movieId + '?api_key=' + this.apiKey + '&language=en-US')
  }

  getCredits(movieId: number): Observable<any> {
    return this.get(this.serviceUrl + movieId.toString() + '/credits?api_key=' + this.apiKey + '&language=en-US')
  }

  getVideos(movieId: number): Observable<any> {
    return this.get(this.serviceUrl + movieId.toString() + '/videos?api_key=' + this.apiKey + '&language=en-US')
  }

  getSimilar(movieId: number): Observable<any> {
    return this.get(this.serviceUrl + movieId.toString() + '/recommendations?api_key=' + this.apiKey + '&language=en-US&page=1')
  }

  getAllReviews(): Observable<any> {
    return this.get(this.localUrl + 'reviews/');
  }

  getOthersReviews(movieId: number): Observable<any> {
    // return this.get(this.serviceUrl + movieId.toString() + '/reviews?api_key=' + this.apiKey + '&language=en-US&page=1')
    return this.get(this.localUrl + 'reviews/' + movieId.toString());
  }

  addUser(data: any) {
    return this.post(this.localUrl + 'addUser', data, {withCredentials: false});
  }

  getOneUser(data: any) {
    return this.http.post(this.localUrl + 'getOneUser', data, {withCredentials: true});
  }

  addReview(data: any) {
    return this.post(this.localUrl + 'addReview', data, {withCredentials: false});
  }

  addToWatchlist(data: any) {
    return this.post(this.localUrl + 'addToWatchlist', data, {withCredentials: false});
  }






  getWatchlist(userId: number) {
    return this.get(this.localUrl + 'watchlist/' + userId.toString());
  }


}
